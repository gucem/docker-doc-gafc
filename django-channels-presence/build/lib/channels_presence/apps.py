from django.apps import AppConfig


class RoomsConfig(AppConfig):
    name = 'channels_presence'
    verbose_name = 'Django Channels Presence'
