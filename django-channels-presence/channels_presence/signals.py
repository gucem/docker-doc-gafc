from django import dispatch

#presence_changed = dispatch.Signal(
#    providing_args=["room", "added", "removed", "bulk_change"]
#)

# No providing_args as from django 3.1
presence_changed = dispatch.Signal()
