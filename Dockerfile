 
FROM python:3.8-slim
ENV PYTHONUNBUFFERED=1
RUN mkdir -p /gafc/static


WORKDIR /gafc
#RUN apk -U upgrade
#RUN apk add gcc g++ make libc-dev libressl-dev musl-dev libffi-dev cargo git curl libcurl curl-dev bash mysql-dev mysql-client jpeg-dev
RUN apt-get update
#RUN apt-get upgrade
RUN apt-get install -y gcc g++ make libc-dev musl-dev libffi-dev cargo git curl
RUN apt-get install -y libcurl4-openssl-dev default-libmysqlclient-dev mariadb-client-10.5 libmagic1
RUN pip install --upgrade setuptools pip

COPY requirements.txt /gafc/
COPY django-channels-presence/ /tmp/django-channels-presence/

RUN pip install -r requirements.txt
RUN pip install /tmp/django-channels-presence

RUN mkdir -p /gafc/gafc
RUN chown nobody /gafc/static
RUN chown nobody /gafc/gafc

USER nobody
RUN mkdir -p /tmp/gafc

ENTRYPOINT ["/bin/bash"]

