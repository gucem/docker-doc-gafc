Django>=4.1,<4.2
channels>=3.0, <4.0
channels-redis>=3.3.1, <4.0
django-crispy-forms==1.11
django-dbbackup>=3.3,<4.0
django-debug-toolbar>=3.2, <4.0
django-utils
GitPython>=3.1,<4.0
uvicorn >= 0.17
redis==4.1
celery>=5.0,<6.0
pycurl>=7.40,<8.0
openpyxl>=3.0,<4.0
dateutils
websockets >= 10.2
mysqlclient>=2.1
numpy>=1.22
Markdown >= 3.3
mdx-linkify >= 2.1
matplotlib >= 3.5
reportlab >= 3.6.9
python-magic
pandas
mysql_server_has_gone_away==2.0.0
sphinx >= 5.0,<6.0

